# Kansas Occupancy Manager

What is it? Take home task for advancing to the interview round with a company.

Why such a name? Secrecy. I don't want other candidates borrow anything from my solution.

## Prerequisites.

* JDK 11.
* Gradle. Run `./gradlew build` and it will make sure appropriate gradle version becomes available.

## Testing

```bash
./gradlew check
```

To run tests in continuous watch mode:

```bash
./gradlew check --continuous
```

## Running

TODO