package com.occupancy.manager;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.joda.money.Money;

/**
 * Information about the hotel necessary to find optimal occupancy.
 */
@Value
@Builder
public class HotelProperties {
  @NonNull Money premiumThreshold;
  int availablePremiumRoomsCount;
  int availableEconomyRoomsCount;
}
