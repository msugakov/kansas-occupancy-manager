package com.occupancy.manager;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import org.joda.money.Money;

/** Suggested optimal occupancy according to the criteria set for {@link OccupancySolver}. */
@Value
@Builder
public class OptimalOccupancy {
  int occupiedPremiumRoomsCount;
  @NonNull Money premiumRoomsRevenue;
  int occupiedEconomyRoomsCount;
  @NonNull Money economyRoomsRevenue;
}
