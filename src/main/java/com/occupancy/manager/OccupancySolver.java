package com.occupancy.manager;

import com.google.common.base.Verify;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.money.MoneyUtils;

/**
 * Finds the maximum revenue a hotel can make for the night according to the problem statement
 * (below). It is assumed that the hotel tries to allocate as many guests as possible.
 *
 * <p>Our hotel clients have two different categories of rooms: Premium and Economy. Our hotels want
 * their customers to be satisfied: they will not book a customer willing to pay EUR 100 or more for
 * the night into an Economy room. But they will book lower paying customers into Premium rooms if
 * these rooms would be empty and all Economy rooms will be filled by low paying customers. Highest
 * paying customers below EUR 100 will get preference for the “upgrade”. Customers always only have
 * one specific price they are willing to pay for the night.
 */
public class OccupancySolver {

  /**
   * Calculates number of occupied rooms for each type (Premium and Economy) based on hotel
   * properties and guest bids.
   */
  public OptimalOccupancy findOptimalOccupancy(
      @NonNull final HotelProperties hotelProperties, @NonNull final List<Money> guestBids) {

    // Premium bids that get Premium room.
    final var premiumBids =
        guestBids.stream()
            .filter(x -> !x.isLessThan(hotelProperties.getPremiumThreshold()))
            .sorted(Comparator.reverseOrder())
            .limit(hotelProperties.getAvailablePremiumRoomsCount())
            .collect(Collectors.toList());

    final int premiumRoomsRemaining =
        hotelProperties.getAvailablePremiumRoomsCount() - premiumBids.size();

    final var allEconomyBids =
        guestBids.stream()
            .filter(x -> x.isLessThan(hotelProperties.getPremiumThreshold()))
            .sorted(Comparator.reverseOrder())
            .collect(Collectors.toList());

    final List<Money> economyTrueBids; // Economy bids who get Economy room.
    final List<Money> economyUpgradeBids; // Economy bids who get upgrade to Premium room.

    if (allEconomyBids.size() > hotelProperties.getAvailableEconomyRoomsCount()) {
      final int upgradableCount =
          Math.min(
              premiumRoomsRemaining,
              allEconomyBids.size() - hotelProperties.getAvailableEconomyRoomsCount());
      economyUpgradeBids = allEconomyBids.subList(0, upgradableCount);
      // Using stream to get sublist below in order not to run into off-by-one issues with indexes
      // math.
      economyTrueBids =
          allEconomyBids.stream()
              .skip(upgradableCount)
              .limit(hotelProperties.getAvailableEconomyRoomsCount())
              .collect(Collectors.toList());
    } else {
      economyUpgradeBids = List.of();
      economyTrueBids = allEconomyBids;
    }

    final int economyCount = economyTrueBids.size();
    Verify.verify(economyCount <= hotelProperties.getAvailableEconomyRoomsCount());
    // TODO: move up and verify?
    final CurrencyUnit currencyUnit = hotelProperties.getPremiumThreshold().getCurrencyUnit();
    final Money economyRevenue = sumMoney(currencyUnit, economyTrueBids);

    final int premiumCount = premiumBids.size() + economyUpgradeBids.size();
    Verify.verify(premiumCount <= hotelProperties.getAvailablePremiumRoomsCount());
    final Money premiumRevenue =
        MoneyUtils.add(
            sumMoney(currencyUnit, premiumBids), sumMoney(currencyUnit, economyUpgradeBids));

    return OptimalOccupancy.builder()
        .occupiedPremiumRoomsCount(premiumCount)
        .premiumRoomsRevenue(premiumRevenue)
        .occupiedEconomyRoomsCount(economyCount)
        .economyRoomsRevenue(economyRevenue)
        .build();
  }

  private static Money sumMoney(final CurrencyUnit currencyUnit, final List<Money> money) {
    return money.stream().reduce(Money.zero(currencyUnit), Money::plus);
  }
}
