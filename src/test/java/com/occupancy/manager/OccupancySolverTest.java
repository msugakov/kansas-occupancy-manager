package com.occupancy.manager;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.junit.jupiter.api.Test;

/** Validates implementation of {@link OccupancySolver}. */
public class OccupancySolverTest {
  private static final CurrencyUnit TEST_CURRENCY = CurrencyUnit.USD;

  // Sample bids from the problem statement. Sorted they are:
  // 374, 209, 155, 115, 101, 100, 99, 45, 23, 22
  private static final List<Money> SAMPLE_BIDS =
      money(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);

  private final OccupancySolver occupancySolver = new OccupancySolver();

  @Test
  public void givenNoRooms_thenNoBusiness() {
    testSample(
        new HotelProperties(money(60), 0, 0),
        money(10, 20, 30),
        new OptimalOccupancy(0, money(0), 0, money(0)));
  }

  @Test
  public void givenOnlyPremiumRooms_whenAllEconomyClients_thenAllClientsSettled() {
    testSample(
        new HotelProperties(money(60), 5, 0),
        money(10, 20, 30),
        new OptimalOccupancy(3, money(60), 0, money(0)));
  }

  @Test
  public void givenOnlyEconomyRooms_whenAllPremiumClients_thenNoClientsSettled() {
    testSample(
        new HotelProperties(money(60), 0, 7),
        money(60, 61, 62),
        new OptimalOccupancy(0, money(0), 0, money(0)));
  }

  @Test
  public void givenOnlyPremiumRooms_whenMixedClients_thenAllClientsAreKings() {
    testSample(
        new HotelProperties(money(60), 50, 0),
        money(10, 20, 30, 60, 61, 62, 63),
        new OptimalOccupancy(7, money(306), 0, money(0)));
  }

  @Test
  public void givenOnlyPremiumRooms_whenMixedClientsAndHighDemand_thenCheaperBidsTakeNoPlace() {
    testSample(
        new HotelProperties(money(60), 5, 0),
        money(10, 20, 30, 60, 61, 62, 63),
        new OptimalOccupancy(5, money(276), 0, money(0)));
  }

  @Test
  public void givenOnlyEconomyRooms_whenAllEconomyClients_thenHighBidsWin() {
    testSample(
        new HotelProperties(money(60), 0, 3),
        money(55, 40, 30, 20, 10, 5),
        new OptimalOccupancy(0, money(0), 3, money(125)));
  }

  @Test
  public void givenRooms_whenNoClients_thenNoBusiness() {
    testSample(
        new HotelProperties(money(60), 90, 200),
        money(),
        new OptimalOccupancy(0, money(0), 0, money(0)));
  }

  // Following are tests from the problem statement. They are quite good, although I decided not
  // to give them special names.

  @Test
  public void givenSampleTest1_thenExpectedResult() {
    testSample(
        new HotelProperties(money(100), 3, 3),
        SAMPLE_BIDS,
        new OptimalOccupancy(3, money(738), 3, money(167)));
  }

  @Test
  public void givenSampleTest2_thenExpectedResult() {
    testSample(
        new HotelProperties(money(100), 7, 5),
        SAMPLE_BIDS,
        new OptimalOccupancy(6, money(1054), 4, money(189)));
  }

  @Test
  public void givenSampleTest3_thenExpectedResult() {
    testSample(
        new HotelProperties(money(100), 2, 7),
        SAMPLE_BIDS,
        new OptimalOccupancy(2, money(583), 4, money(189)));
  }

  @Test
  public void givenSampleTest4_thenExpectedResult() {
    testSample(
        new HotelProperties(money(100), 7, 1),
        SAMPLE_BIDS,
        new OptimalOccupancy(7, money(1153), 1, money(45)));
  }

  private void testSample(
      final HotelProperties hotelProperties,
      final List<Money> bids,
      final OptimalOccupancy expectedResult) {
    final OptimalOccupancy result = occupancySolver.findOptimalOccupancy(hotelProperties, bids);
    assertThat(result).isEqualTo(expectedResult);
  }

  private static Money money(final int amount) {
    return Money.of(TEST_CURRENCY, BigDecimal.valueOf(amount));
  }

  private static List<Money> money(final int... amounts) {
    return IntStream.of(amounts)
        .boxed()
        .map(OccupancySolverTest::money)
        .collect(Collectors.toList());
  }
}
